#include "threads.h"

void I_Love_Threads()
{
	cout << "I Love Threads" << endl;
}

void call_I_Love_Threads()
{
	std::thread th(I_Love_Threads);
	th.join();
}

void printVector(vector<int> primes)
{
	std::vector<int>::iterator it;
	for (it = primes.begin(); it<primes.end(); it++)
		cout << *it << endl; 
}

void getPrimes(int begin, int end, vector<int>& primes)
{
	int flag, i;
	if (begin < 2)
		begin = 2;
	while (begin < end)
	{
		flag = 0;

		for (i = 2; i <= begin / 2; ++i)
		{
			if (begin % i == 0)
			{
				flag = 1;
				break;
			}
		}

		if (flag == 0)
			primes.push_back(begin);

		++begin;
	}
}

vector<int> callGetPrimes(int begin, int end)
{
	vector<int> primes;
	thread th(getPrimes, begin, end ,ref(primes));
	auto start = chrono::high_resolution_clock::now();
	th.join();
	auto finish = chrono::high_resolution_clock::now();
	chrono::duration<double> elapsed = finish - start;
	cout << "Elapsed time: " << elapsed.count() << " s\n";
	return primes;
}

void writePrimesToFile(int begin, int end, ofstream & file)
{
	int flag, i;
	if (begin < 2)
		begin = 2;
	while (begin < end)
	{
		flag = 0;

		for (i = 2; i <= begin / 2; ++i)
		{
			if (begin % i == 0)
			{
				flag = 1;
				break;
			}
		}

		if (flag == 0)
			file << endl << begin << endl;

		++begin;
	}
	
}

void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N)
{
	thread * myThreads = new thread[N];
	ofstream ofs;
	int sizeOfThread = (begin + end) / N;
	ofs.open(filePath);

	for (int i = 0; i < N; ++i)
	{
		myThreads[i] = thread(writePrimesToFile,(i * sizeOfThread + begin), ((i * sizeOfThread + begin) + sizeOfThread), ref(ofs));
	}

	for (int i = 0; i < N; ++i)
	{
		myThreads[i].join();
	}

	delete[] myThreads;
	ofs.close();
}
